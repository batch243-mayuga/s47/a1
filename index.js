// console.log("Congrats");

// Section: Document Object Model (DOM)
	// allows us to access or modify the proporties of an HTML element in a webpage
	// it is standard on how to get, change, add or delete HTML elements
	// we will focus on use of DOM in managing forms.

// For selecting HTML elements we will be using document.querySelector
	// Syntax: document.querySelector("html element")
	// the querySelector function takes a string input that is formatted like a css selector when applying the styles.

	const txtFirstName = document.querySelector("#txt-first-name"); 
	console.log(txtFirstName);

	const txtLastName = document.querySelector("#txt-last-name");

	const name = document.querySelectorAll(".full-name");
	console.log(name);

	const span = document.querySelectorAll("span");
	console.log(span);

	const text = document.querySelectorAll("input[type]");
	console.log(text);

	const spanFullname = document.querySelector("#fullName")

// Section: Event Listeners
	// whenever a user interacts with a webpage, this action is considered as an event.
	// working with events is large part of creating interactivity in a webpage.
	// specific functions that will perform an action

// The function use is "addEventListener" it takes two arguments
	// first argument a string identifying an event.
	// second argument, function that the listener will trigger once the "specified event" is triggered.

	txtFirstName.addEventListener("keyup", (event) => {
		console.log(event.target.value);
	})

	txtLastName.addEventListener("keyup", (event) => {
		console.log(event.target.value);
	})

	// keyup, keydown, change
	// event.target.value - getting the value.

	const changeColor = document.querySelector("#colorChange");

	const fullName = () => {
		spanFullname.innerHTML = `${txtFirstName.value} ${txtLastName.value}`
	}

	txtFirstName.addEventListener("keyup", fullName);
	txtLastName.addEventListener("keyup", fullName);

	const colors = () => {
		spanFullname.style.color = `${changeColor.value}`
	}

	changeColor.addEventListener("click", colors);